READ `doc/privacy` FOR MORE DETAILS.

# Welcome to my Hardened Firefox Configuration!

Installation
------------

* Before the installation, you should open Firefox at least once.
 Also, it's preferable to remove all data in your profile
 (the one that you want to install this configuration to).

**NOTE**: Make sure `aria2c`, `bash`, and `realpath` is in your PATH variable,
 if not then install them first.

Installation:
```
$ git clone https://github.com/ides3rt/Firefox
$ Firefox/src/user-js.sh
$ mv Firefox/user.js ~/.mozilla/firefox/<FF_PROF>
```

Recommendations
---------------

* [My Firefox extensions](https://codeberg.org/ides3rt/Firefox/src/branch/main/doc).
* [Arkenfox's wiki](https://github.com/arkenfox/user.js/wiki).
