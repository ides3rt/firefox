Links To The Extensions
-----------------------

**NOTES**: The directories are the settings for each extensions.

* [CanvasBlocker](https://addons.mozilla.org/en-US/firefox/addon/canvasblocker/)
* [Skip Redirect](https://addons.mozilla.org/en-US/firefox/addon/skip-redirect/)
* [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
